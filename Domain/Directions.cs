﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Domain
{
    public enum Directions
    {
        S  = 0,
        E  = 1,
        N  = 2,
        W  = 3
    }
}
