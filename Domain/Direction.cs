﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Domain
{
    public class Direction
    {
        private int _index;

        public Directions direction { get; private set; }

        public Direction(Directions startDirection)
        {
            this._index = (int)startDirection;
            this.direction = startDirection;
        }

        public Directions Turn(char turn)
        {
            if (turn.Equals('L'))
                _index = _index + 1;
            if (turn.Equals('R'))
                _index = _index - 1;

            if (_index > 3) { _index = 0; }
            if (_index < 0) { _index = 3; }

            direction = (Directions)_index;

            return direction;
        }


    }
}
