﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Domain
{
    public class Drone
    {
        private Direction _direction;

        private readonly int _startX;
        private readonly int _startY;

        private readonly int _areaX;
        private readonly int _areaY;

        private int currentX;
        private int currentY;

        private string _moveOutsideOfArea = string.Empty;

        public Drone(int startX, int startY, int areaX, int areaY, Directions startDirection)
        {
            if (startX > areaX || startY > areaY)
                throw new ArgumentOutOfRangeException("Drone start point is outside of allowed area.");
             
            this._direction = new Direction(startDirection);

            this._startX = startX;
            this._startY = startY;

            this._areaX = areaX;
            this._areaY = areaY;

            this.currentX = startX;
            this.currentY = startY;
    }

        public string CalculateLastPosition(string commandsLine)
        {
            List<char> commands = commandsLine.ToList();

            commands.ForEach(c => ExecuteCommand(c));

            return _moveOutsideOfArea + currentX.ToString() + " " + currentY.ToString()
                + " " + _direction.direction.ToString();
        }

        private void ExecuteCommand(char command)
        {
            if(command.Equals('L') || command.Equals('R'))
                _direction.Turn(command);

            if (command.Equals('M'))
            {
                Move();
                isMoveOutsideOfArea();
            }
                           
        }

        private void Move()
        {
            switch (_direction.direction)
            {
                case Directions.S: currentY--; break;
                case Directions.E: currentX++; break;
                case Directions.N: currentY++; break;
                case Directions.W: currentX--; break;
            }
        }

        private void isMoveOutsideOfArea()
        {
            if (currentX > _areaX || currentY > _areaY ||
                currentX < 0 || currentY < 0)
                _moveOutsideOfArea = "One of the Moves was outside of area. ";
        }
    }
}
