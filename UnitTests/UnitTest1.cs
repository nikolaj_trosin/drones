﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Domain;

namespace UnitTests
{
    [TestClass]
    public class UnitTest1
    {
        [TestMethod]
        public void MakeRightTurnFromEast()
        {
            Direction direction = new Direction(Directions.E);
            Directions fromTurn =  direction.Turn('R');

            Assert.AreEqual(Directions.S, fromTurn);
        }

        [TestMethod]
        public void MakeRightTurnFromSouth()
        {
            Direction direction = new Direction(Directions.S);
            Directions fromTurn = direction.Turn('R');

            Assert.AreEqual(Directions.W, fromTurn);
        }

        [TestMethod]
        public void TestFirstDrone()
        {
            Drone drone = new Drone(3, 3, 5, 5, Directions.E);
            string lastPosition = drone.CalculateLastPosition("L");

            Assert.AreEqual("3 3 N", lastPosition);
        }

        [TestMethod]
        public void TestSecondDrone()
        {
            Drone drone = new Drone(3, 3, 5, 5, Directions.E);
            string lastPosition = drone.CalculateLastPosition("MMRMMRMRRM");

            Assert.AreEqual("5 1 E", lastPosition);
        }

        [TestMethod]
        public void TestThirdDrone()
        {
            Drone drone = new Drone(1, 2, 5, 5, Directions.N);
            string lastPosition = drone.CalculateLastPosition("LMLMLMLMMLMLMLMLMM");

            Assert.AreEqual("1 4 N", lastPosition);
        }

        [TestMethod]
        public void TestStartOutsideOfArea()
        {
            Assert.ThrowsException< ArgumentOutOfRangeException>(() => new Drone(5, 6, 5, 5, Directions.N), 
                "Drone start point is outside of allowed area.");
        }

        [TestMethod]
        public void TestMoveOutsideOfArea()
        {
            Drone drone = new Drone(5, 5, 5, 5, Directions.N);
            string lastPosition = drone.CalculateLastPosition("M");

            Assert.IsTrue(lastPosition.Contains("One of the Moves was outside of area. "));
        }
    }
}
