﻿namespace UI
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.areaLabel = new System.Windows.Forms.Label();
            this.areaX = new System.Windows.Forms.NumericUpDown();
            this.areaY = new System.Windows.Forms.NumericUpDown();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.Drone1X = new System.Windows.Forms.NumericUpDown();
            this.Drone1Y = new System.Windows.Forms.NumericUpDown();
            this.commands1 = new System.Windows.Forms.TextBox();
            this.commands2 = new System.Windows.Forms.TextBox();
            this.Drone2Y = new System.Windows.Forms.NumericUpDown();
            this.Drone2X = new System.Windows.Forms.NumericUpDown();
            this.label10 = new System.Windows.Forms.Label();
            this.commands3 = new System.Windows.Forms.TextBox();
            this.Drone3Y = new System.Windows.Forms.NumericUpDown();
            this.Drone3X = new System.Windows.Forms.NumericUpDown();
            this.label14 = new System.Windows.Forms.Label();
            this.Calculate = new System.Windows.Forms.Button();
            this.direction = new System.Windows.Forms.Label();
            this.direction1 = new System.Windows.Forms.ListBox();
            this.direction2 = new System.Windows.Forms.ListBox();
            this.direction3 = new System.Windows.Forms.ListBox();
            this.label6 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.output1 = new System.Windows.Forms.Label();
            this.output2 = new System.Windows.Forms.Label();
            this.output3 = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.errorProvider1 = new System.Windows.Forms.ErrorProvider(this.components);
            ((System.ComponentModel.ISupportInitialize)(this.areaX)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.areaY)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Drone1X)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Drone1Y)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Drone2Y)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Drone2X)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Drone3Y)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Drone3X)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.errorProvider1)).BeginInit();
            this.SuspendLayout();
            // 
            // areaLabel
            // 
            this.areaLabel.AutoSize = true;
            this.areaLabel.Location = new System.Drawing.Point(197, 37);
            this.areaLabel.Name = "areaLabel";
            this.areaLabel.Size = new System.Drawing.Size(32, 13);
            this.areaLabel.TabIndex = 0;
            this.areaLabel.Text = "Area:";
            // 
            // areaX
            // 
            this.areaX.Location = new System.Drawing.Point(251, 35);
            this.areaX.Minimum = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.areaX.Name = "areaX";
            this.areaX.Size = new System.Drawing.Size(50, 20);
            this.areaX.TabIndex = 1;
            this.areaX.Value = new decimal(new int[] {
            1,
            0,
            0,
            0});
            // 
            // areaY
            // 
            this.areaY.Location = new System.Drawing.Point(334, 35);
            this.areaY.Minimum = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.areaY.Name = "areaY";
            this.areaY.Size = new System.Drawing.Size(54, 20);
            this.areaY.TabIndex = 2;
            this.areaY.Value = new decimal(new int[] {
            1,
            0,
            0,
            0});
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(264, 16);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(14, 13);
            this.label1.TabIndex = 3;
            this.label1.Text = "X";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(352, 16);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(14, 13);
            this.label2.TabIndex = 4;
            this.label2.Text = "Y";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(6, 108);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(46, 13);
            this.label3.TabIndex = 5;
            this.label3.Text = "1 drone:";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(80, 83);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(37, 13);
            this.label4.TabIndex = 6;
            this.label4.Text = "start X";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(145, 83);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(37, 13);
            this.label5.TabIndex = 7;
            this.label5.Text = "start Y";
            // 
            // Drone1X
            // 
            this.Drone1X.Location = new System.Drawing.Point(71, 106);
            this.Drone1X.Name = "Drone1X";
            this.Drone1X.Size = new System.Drawing.Size(45, 20);
            this.Drone1X.TabIndex = 8;
            // 
            // Drone1Y
            // 
            this.Drone1Y.Location = new System.Drawing.Point(149, 106);
            this.Drone1Y.Name = "Drone1Y";
            this.Drone1Y.Size = new System.Drawing.Size(54, 20);
            this.Drone1Y.TabIndex = 9;
            // 
            // commands1
            // 
            this.commands1.Location = new System.Drawing.Point(287, 106);
            this.commands1.Name = "commands1";
            this.commands1.Size = new System.Drawing.Size(226, 20);
            this.commands1.TabIndex = 11;
            this.commands1.TextChanged += new System.EventHandler(this.commands1_TextChanged);
            // 
            // commands2
            // 
            this.commands2.Location = new System.Drawing.Point(287, 153);
            this.commands2.Name = "commands2";
            this.commands2.Size = new System.Drawing.Size(226, 20);
            this.commands2.TabIndex = 18;
            this.commands2.TextChanged += new System.EventHandler(this.commands2_TextChanged);
            // 
            // Drone2Y
            // 
            this.Drone2Y.Location = new System.Drawing.Point(149, 151);
            this.Drone2Y.Name = "Drone2Y";
            this.Drone2Y.Size = new System.Drawing.Size(54, 20);
            this.Drone2Y.TabIndex = 16;
            // 
            // Drone2X
            // 
            this.Drone2X.Location = new System.Drawing.Point(71, 151);
            this.Drone2X.Name = "Drone2X";
            this.Drone2X.Size = new System.Drawing.Size(45, 20);
            this.Drone2X.TabIndex = 15;
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Location = new System.Drawing.Point(6, 153);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(46, 13);
            this.label10.TabIndex = 12;
            this.label10.Text = "2 drone:";
            // 
            // commands3
            // 
            this.commands3.Location = new System.Drawing.Point(287, 205);
            this.commands3.Name = "commands3";
            this.commands3.Size = new System.Drawing.Size(226, 20);
            this.commands3.TabIndex = 25;
            this.commands3.TextChanged += new System.EventHandler(this.commands3_TextChanged);
            // 
            // Drone3Y
            // 
            this.Drone3Y.Location = new System.Drawing.Point(149, 203);
            this.Drone3Y.Name = "Drone3Y";
            this.Drone3Y.Size = new System.Drawing.Size(54, 20);
            this.Drone3Y.TabIndex = 23;
            // 
            // Drone3X
            // 
            this.Drone3X.Location = new System.Drawing.Point(72, 203);
            this.Drone3X.Name = "Drone3X";
            this.Drone3X.Size = new System.Drawing.Size(45, 20);
            this.Drone3X.TabIndex = 22;
            // 
            // label14
            // 
            this.label14.AutoSize = true;
            this.label14.Location = new System.Drawing.Point(6, 205);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(46, 13);
            this.label14.TabIndex = 19;
            this.label14.Text = "3 drone:";
            // 
            // Calculate
            // 
            this.Calculate.Location = new System.Drawing.Point(235, 306);
            this.Calculate.Name = "Calculate";
            this.Calculate.Size = new System.Drawing.Size(75, 23);
            this.Calculate.TabIndex = 26;
            this.Calculate.Text = "Calculate:";
            this.Calculate.UseVisualStyleBackColor = true;
            this.Calculate.Click += new System.EventHandler(this.Calculate_Click);
            // 
            // direction
            // 
            this.direction.AutoSize = true;
            this.direction.Location = new System.Drawing.Point(232, 83);
            this.direction.Name = "direction";
            this.direction.Size = new System.Drawing.Size(47, 13);
            this.direction.TabIndex = 28;
            this.direction.Text = "direction";
            // 
            // direction1
            // 
            this.direction1.FormattingEnabled = true;
            this.direction1.Location = new System.Drawing.Point(234, 108);
            this.direction1.Name = "direction1";
            this.direction1.Size = new System.Drawing.Size(44, 30);
            this.direction1.TabIndex = 29;
            // 
            // direction2
            // 
            this.direction2.FormattingEnabled = true;
            this.direction2.Location = new System.Drawing.Point(234, 151);
            this.direction2.Name = "direction2";
            this.direction2.Size = new System.Drawing.Size(44, 30);
            this.direction2.TabIndex = 30;
            // 
            // direction3
            // 
            this.direction3.FormattingEnabled = true;
            this.direction3.Location = new System.Drawing.Point(235, 193);
            this.direction3.Name = "direction3";
            this.direction3.Size = new System.Drawing.Size(44, 30);
            this.direction3.TabIndex = 31;
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(374, 83);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(37, 13);
            this.label6.TabIndex = 10;
            this.label6.Text = "Move:";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(527, 83);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(42, 13);
            this.label7.TabIndex = 32;
            this.label7.Text = "Output:";
            // 
            // output1
            // 
            this.output1.AutoSize = true;
            this.output1.Location = new System.Drawing.Point(527, 113);
            this.output1.Name = "output1";
            this.output1.Size = new System.Drawing.Size(0, 13);
            this.output1.TabIndex = 33;
            // 
            // output2
            // 
            this.output2.AutoSize = true;
            this.output2.Location = new System.Drawing.Point(527, 160);
            this.output2.Name = "output2";
            this.output2.Size = new System.Drawing.Size(0, 13);
            this.output2.TabIndex = 34;
            // 
            // output3
            // 
            this.output3.AutoSize = true;
            this.output3.Location = new System.Drawing.Point(527, 212);
            this.output3.Name = "output3";
            this.output3.Size = new System.Drawing.Size(0, 13);
            this.output3.TabIndex = 35;
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(331, 297);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(0, 13);
            this.label8.TabIndex = 36;
            // 
            // errorProvider1
            // 
            this.errorProvider1.ContainerControl = this;
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(754, 374);
            this.Controls.Add(this.label8);
            this.Controls.Add(this.output3);
            this.Controls.Add(this.output2);
            this.Controls.Add(this.output1);
            this.Controls.Add(this.label7);
            this.Controls.Add(this.direction3);
            this.Controls.Add(this.direction2);
            this.Controls.Add(this.direction1);
            this.Controls.Add(this.direction);
            this.Controls.Add(this.Calculate);
            this.Controls.Add(this.commands3);
            this.Controls.Add(this.Drone3Y);
            this.Controls.Add(this.Drone3X);
            this.Controls.Add(this.label14);
            this.Controls.Add(this.commands2);
            this.Controls.Add(this.Drone2Y);
            this.Controls.Add(this.Drone2X);
            this.Controls.Add(this.label10);
            this.Controls.Add(this.commands1);
            this.Controls.Add(this.label6);
            this.Controls.Add(this.Drone1Y);
            this.Controls.Add(this.Drone1X);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.areaY);
            this.Controls.Add(this.areaX);
            this.Controls.Add(this.areaLabel);
            this.Name = "Form1";
            this.Text = "Form1";
            this.Load += new System.EventHandler(this.Form1_Load);
            ((System.ComponentModel.ISupportInitialize)(this.areaX)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.areaY)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Drone1X)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Drone1Y)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Drone2Y)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Drone2X)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Drone3Y)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Drone3X)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.errorProvider1)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label areaLabel;
        private System.Windows.Forms.NumericUpDown areaX;
        private System.Windows.Forms.NumericUpDown areaY;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.NumericUpDown Drone1X;
        private System.Windows.Forms.NumericUpDown Drone1Y;
        private System.Windows.Forms.TextBox commands1;
        private System.Windows.Forms.TextBox commands2;
        private System.Windows.Forms.NumericUpDown Drone2Y;
        private System.Windows.Forms.NumericUpDown Drone2X;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.TextBox commands3;
        private System.Windows.Forms.NumericUpDown Drone3Y;
        private System.Windows.Forms.NumericUpDown Drone3X;
        private System.Windows.Forms.Label label14;
        private System.Windows.Forms.Button Calculate;
        private System.Windows.Forms.Label direction;
        private System.Windows.Forms.ListBox direction1;
        private System.Windows.Forms.ListBox direction2;
        private System.Windows.Forms.ListBox direction3;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label output1;
        private System.Windows.Forms.Label output2;
        private System.Windows.Forms.Label output3;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.ErrorProvider errorProvider1;
    }
}

