﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Domain;

namespace UI
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
            direction1.DataSource = System.Enum.GetValues(typeof(Directions));
            direction2.DataSource = System.Enum.GetValues(typeof(Directions));
            direction3.DataSource = System.Enum.GetValues(typeof(Directions));
        }

        private void Calculate_Click(object sender, EventArgs e)
        {
            label8.Text = string.Empty;
            try
            {

                Drone drone1 = new Drone((int)Drone1X.Value, (int)Drone1Y.Value, (int)areaX.Value, (int)areaY.Value, (Directions)direction1.SelectedItem);
                Drone drone2 = new Drone((int)Drone2X.Value, (int)Drone2Y.Value, (int)areaX.Value, (int)areaY.Value, (Directions)direction2.SelectedItem);
                Drone drone3 = new Drone((int)Drone3X.Value, (int)Drone3Y.Value, (int)areaX.Value, (int)areaY.Value, (Directions)direction3.SelectedItem);

                output1.Text = drone1.CalculateLastPosition(commands1.Text);
                output2.Text = drone2.CalculateLastPosition(commands2.Text);
                output3.Text = drone3.CalculateLastPosition(commands3.Text);
            }
            catch (Exception ex)
            {
                label8.Text = ex.Message;
            }
        }

        private bool ValidateCommands(string commands)
        {
            bool isValid = commands.All(c => char.IsLetter(c) && c == 'M' || c == 'L' || c == 'R');
            return isValid;
        }

        private void Form1_Load(object sender, EventArgs e)
        {
            
        }

        private void commands1_TextChanged(object sender, EventArgs e)
        {
            bool isValid = ValidateCommands(commands1.Text);
            if (!isValid)
            {
                errorProvider1.SetError(commands1, "Must be only: L or M or R");
                Calculate.Enabled = false;
            }
            else
            {
                Calculate.Enabled = true;
                errorProvider1.Clear();
            }
        }

        private void commands2_TextChanged(object sender, EventArgs e)
        {
            bool isValid = ValidateCommands(commands2.Text);
            if (!isValid)
            {
                errorProvider1.SetError(commands2, "Must be only: L or M or R");
                Calculate.Enabled = false;
            }
            else
            {
                errorProvider1.Clear();
                Calculate.Enabled = true;
            }
        }

        private void commands3_TextChanged(object sender, EventArgs e)
        {
            bool isValid = ValidateCommands(commands3.Text);
            if (!isValid)
            {
                errorProvider1.SetError(commands3, "Must be only: L or M or R");
                Calculate.Enabled = false;
            }
            else
            {
                errorProvider1.Clear();
                Calculate.Enabled = true;
            }
        }
    }
}
